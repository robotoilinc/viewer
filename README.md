# Viewer

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/B0B7CTDVS)

A simple Userscript that allows you to see uploaded QC's.

# Supported

- 1688
- Taobao
- Tmall
- Yupoo
- Weidian

# Uploading

You can upload QC's using [FR:Reborn - Agents extension](https://greasyfork.org/en/scripts/426977-fr-reborn-agents-extension) and also view them via the [FR:Reborn website](https://www.fashionreps.page/recent).


# Support me

Make sure to use my [BaseTao affiliate link](https://www.basetao.com/?KmIRcayhHuIWoa4NAw) when signing up for an account. You instantly get 2500 membership points!
