import { elementReady } from '../helpers/waitFor';

export default class SourceWeidian {
  constructor() {
    this.setup = false;
  }

  /**
   * @param client {SwaggerClient}
   * @returns {SourceWeidian}
   */
  build(client) {
    // If already build before, just return
    if (this.setup) {
      return this;
    }

    this.client = client;
    this.setup = true;

    return this;
  }

  loadIframe($iframe) {
    if (this.setup === false) {
      throw new Error('Source is not setup, so cannot be used');
    }

    const id = window.location.href.match(/[?&]itemId=(\d+)/i)[1];

    // Build URL
    const request = SwaggerClient.buildRequest({
      spec: this.client.spec,
      operationId: 'viewWeidian',
      parameters: { id },
      responseContentType: 'application/json',
    });

    // Hide, to later show and add the src
    $iframe.hide()
      .css('width', '100%')
      .attr('src', request.url);

    // Finally append (once it exists)
    elementReady('.item-info > .item-wrap')
      .then((element) => { $(element).after($iframe); });
  }

  /**
   * @param hostname {string}
   * @returns {boolean}
   */
  supports(hostname) {
    // If we are indeed on Weidian, add specific Weidian CSS
    if (hostname.includes('weidian.com')) {
      GM_addStyle('.swal2-icon,.swal2-icon .swal2-icon-content,.swal2-popup.swal2-toast .swal2-title,.swal2-toast-shown .swal2-container{font-size:12px}.swal2-popup.swal2-toast{padding:.2em}.swal2-timer-progress-bar-container{height:.1em}.swal2-container{height:100px}');

      return true;
    }

    return false;
  }
}
