import { elementReady } from '../helpers/waitFor';

export default class SourceYupoo {
  constructor() {
    this.setup = false;
  }

  /**
   * @param client {SwaggerClient}
   * @returns {SourceYupoo}
   */
  build(client) {
    // If already build before, just return
    if (this.setup) {
      return this;
    }

    this.client = client;
    this.setup = true;

    return this;
  }

  loadIframe($iframe) {
    if (this.setup === false) {
      throw new Error('Source is not setup, so cannot be used');
    }

    const id = window.location.href.match(/^https?:\/\/.*\.x\.yupoo\.com\/albums\/(\d+)/)[1];
    const author = window.location.hostname.replace('.x.yupoo.com', '');

    // Build URL
    const request = SwaggerClient.buildRequest({
      spec: this.client.spec,
      operationId: 'viewYupoo',
      parameters: { id, author },
      responseContentType: 'application/json',
    });

    // Hide, to later show and add the src
    $iframe.hide()
      .attr('src', request.url);

    // Finally append (once it exists)
    elementReady('.showalbum__imagecardwrap')
      .then((element) => { $(element).append($iframe); });
  }

  /**
   * @param hostname {string}
   * @returns {boolean}
   */
  supports(hostname) {
    return hostname.includes('yupoo.com');
  }
}
