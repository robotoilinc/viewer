import { elementReady } from '../helpers/waitFor';

export default class Source1688 {
  constructor() {
    this.setup = false;
  }

  /**
   * @param client {SwaggerClient}
   * @returns {Source1688}
   */
  build(client) {
    // If already build before, just return
    if (this.setup) {
      return this;
    }

    this.client = client;
    this.setup = true;

    return this;
  }

  loadIframe($iframe) {
    if (this.setup === false) {
      throw new Error('Source is not setup, so cannot be used');
    }

    const id = window.location.href.match(/offer\/(\d+)/i)[1];

    // Build URL
    const request = SwaggerClient.buildRequest({
      spec: this.client.spec,
      operationId: 'view1688',
      parameters: { id },
      responseContentType: 'application/json',
    });

    $iframe.hide()
      .css('width', '100%')
      .attr('src', request.url);

    // Finally, append (once it exists)
    elementReady('.od-pc-attribute')
      .then((element) => { $(element).after($iframe); });
  }

  /**
   * @param hostname {string}
   * @returns {boolean}
   */
  supports(hostname) {
    return hostname.includes('1688.com');
  }
}
