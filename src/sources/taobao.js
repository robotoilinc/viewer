import { elementReady } from '../helpers/waitFor';

export default class SourceTaobao {
  constructor() {
    this.setup = false;
  }

  /**
   * @param client {SwaggerClient}
   * @returns {SourceTaobao}
   */
  build(client) {
    // If already build before, just return
    if (this.setup) {
      return this;
    }

    this.client = client;
    this.setup = true;

    return this;
  }

  loadIframe($iframe) {
    if (this.setup === false) {
      throw new Error('Source is not setup, so cannot be used');
    }

    const searchParams = new URLSearchParams(window.location.search);
    const id = searchParams.get('id');

    // Build URL
    const request = SwaggerClient.buildRequest({
      spec: this.client.spec,
      operationId: 'viewTaobao',
      parameters: { id },
      responseContentType: 'application/json',
    });

    // Hide, to later show and add the src
    $iframe.hide()
      .css('width', '100%')
      .attr('src', request.url);

    // Finally append (once it exists)
    elementReady('[class^=BasicContent--root]')
      .then((element) => { $(element).after($iframe); });
  }

  /**
   * @param hostname {string}
   * @returns {boolean}
   */
  supports(hostname) {
    return hostname.includes('taobao');
  }
}
