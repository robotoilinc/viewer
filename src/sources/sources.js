import Source1688 from './1688';
import SourceTaobao from './taobao';
import SourceTmall from './tmall';
import SourceWeidian from './weidian';
import SourceXianyu from './xianyu';
import SourceYupoo from './yupoo';

export class Sources {
  /**
   * @param hostname {string}
   */
  constructor(hostname) {
    this.source = null;

    const sources = [new Source1688(), new SourceTaobao(), new SourceTmall(), new SourceYupoo(), new SourceWeidian(), new SourceXianyu()];
    Object.values(sources).forEach((value) => {
      if (value.supports(hostname)) {
        this.source = value;
      }
    });
  }
}
