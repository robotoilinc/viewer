import { Snackbar } from './helpers/snackbar';
import { Sources } from './sources/sources';

// Inject snackbar css style
GM_addStyle(GM_getResourceText('sweetalert2'));
GM_addStyle('.swal2-container {z-index: 2147483646}');

// Setup proper settings menu
GM_config.init('Settings', {
  serverSection: {
    label: 'QC Server settings',
    type: 'section',
  },
  swaggerDocUrl: {
    label: 'Swagger documentation URL',
    type: 'text',
    default: 'https://www.fashionreps.page/api/doc.json',
  },
});

// Reload page if config changed
GM_config.onclose = (saveFlag) => {
  if (saveFlag) {
    window.location.reload();
  }
};

// Register menu within GM
GM_registerMenuCommand('Settings', GM_config.open);

// eslint-disable-next-line func-names
(async function () {
  // Setup the logger.
  Logger.useDefaults();

  // Log the start of the script.
  Logger.info(`Starting extension '${GM_info.script.name}', version ${GM_info.script.version}`);

  // Get the proper source view, if any
  const { source } = new Sources(window.location.hostname);

  // If we don't have a source, abort
  if (source === null) {
    Logger.error('Unsupported website');

    return;
  }

  /** @type {SwaggerClient} */
  let client;

  // Try to create Swagger client from our own documentation
  try {
    client = await new SwaggerClient({ url: GM_config.get('swaggerDocUrl') });
  } catch (error) {
    Snackbar('We are unable to connect to FR:Reborn, features will be disabled.');
    Logger.error(`We are unable to connect to FR:Reborn: ${GM_config.get('swaggerDocUrl')}`, error);

    return;
  }

  // Create iFrame we will pass to the loader
  const $iframe = $('<iframe id="qciframe" style="border:0" />');
  $iframe.on('load', () => {
    $iframe.iFrameResize({ checkOrigin: ['https://localhost:8000', 'https://www.fashionreps.page', 'https://fashionreps.page'], bodyMargin: 10, bodyPadding: 10 });
    $iframe.show();
  });

  // Build the source and load the iFrame
  source.build(client).loadIframe($iframe);
}());
